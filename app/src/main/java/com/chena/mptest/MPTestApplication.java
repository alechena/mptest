package com.chena.mptest;

import android.app.Application;

import com.chena.mptest.dagger.component.DaggerNetComponent;
import com.chena.mptest.dagger.component.NetComponent;
import com.chena.mptest.dagger.module.AppModule;
import com.chena.mptest.dagger.module.NetModule;
import com.chena.mptest.utils.Constants;

/**
 * Created by Alejandro Chena on 19/02/2018.
 */

public class MPTestApplication extends Application{

    private static MPTestApplication instance;
    private NetComponent mNetComponent;

    public static MPTestApplication getInstance(){
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        mNetComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(Constants.BASE_URL))
                .build();
    }

    public NetComponent getNetComponent() {
        return mNetComponent;
    }
}
