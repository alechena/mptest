package com.chena.mptest.model;

import com.chena.mptest.MPTestApplication;
import com.chena.mptest.content.Bank;
import com.chena.mptest.content.Payment;
import com.chena.mptest.presenter.BankActivityPresenter;
import com.chena.mptest.retrofit.interfaces.BankApiInterface;
import com.chena.mptest.utils.Constants;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public class BankActivityModel {

    private BankActivityPresenter presenter;
    @Inject Retrofit retrofit;

    public BankActivityModel(BankActivityPresenter presenter) {
        this.presenter = presenter;
        MPTestApplication.getInstance().getNetComponent().inject(this);
    }

    public void getBanks(final Payment payment){
        presenter.onRequestStarted();

        BankApiInterface paymentMethodApiInterface
                = retrofit.create(BankApiInterface.class);

        Call<ArrayList<Bank>> call = paymentMethodApiInterface.
                getPaymentMethods(payment.getPaymentMethod().getId());

        call.enqueue(new Callback<ArrayList<Bank>>() {
            @Override
            public void onResponse(Call<ArrayList<Bank>> call, Response<ArrayList<Bank>> response) {
                if ((response.code() == Constants.STATUS_OK) && (response.body() != null)) {
                    if(response.body().size() != 0)
                        presenter.onSuccess(response.body());
                    else {
                        payment.setBank(null);
                        presenter.onEmptyResult();
                    }
                }else
                    presenter.onError();
            }

            @Override
            public void onFailure(Call<ArrayList<Bank>> call, Throwable t) {
                presenter.onError();
            }
        });
    }
}
