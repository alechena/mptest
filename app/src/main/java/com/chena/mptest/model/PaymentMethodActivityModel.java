package com.chena.mptest.model;

import com.chena.mptest.MPTestApplication;
import com.chena.mptest.content.Payment;
import com.chena.mptest.content.PaymentMethod;
import com.chena.mptest.presenter.PaymentMethodActivityPresenter;
import com.chena.mptest.retrofit.interfaces.PaymentMethodApiInterface;
import com.chena.mptest.utils.Constants;
import com.chena.mptest.utils.Functions;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Alejandro Chena on 19/02/2018.
 */

public class PaymentMethodActivityModel {

    private PaymentMethodActivityPresenter presenter;
    @Inject Retrofit retrofit;

    public PaymentMethodActivityModel(PaymentMethodActivityPresenter presenter) {
        this.presenter = presenter;
        MPTestApplication.getInstance().getNetComponent().inject(this);
    }

    public void getPaymentMethods(final Payment payment) {
        presenter.onRequestStarted();
        final int amount = Functions.convertAmountToInt(payment.getAmount());
        PaymentMethodApiInterface paymentMethodApiInterface
                = retrofit.create(PaymentMethodApiInterface.class);

        Call<ArrayList<PaymentMethod>> call = paymentMethodApiInterface.getPaymentMethods();

        call.enqueue(new Callback<ArrayList<PaymentMethod>>() {
            @Override
            public void onResponse(Call<ArrayList<PaymentMethod>> call, Response<ArrayList<PaymentMethod>> response) {
                if ((response.code() == Constants.STATUS_OK) && (response.body() != null)) {
                    int maxAmount = cleanListStatusMaxAmount(response.body(), amount);
                    if(response.body().size()>0)
                        presenter.onSuccess(response.body());
                    else
                        presenter.onEmptyResult(String.valueOf(maxAmount));
                }else
                    presenter.onError();
            }

            @Override
            public void onFailure(Call<ArrayList<PaymentMethod>> call, Throwable t) {
                presenter.onError();
            }
        });
    }
    /**
     * Remove payment methods inactive and amount exceeded
     * @return  Max amount allowed
     * */
    private int cleanListStatusMaxAmount(ArrayList<PaymentMethod> list, int amount){
        int maxAmount = 0;
        List<PaymentMethod> toRemove = new ArrayList<>();
        for(PaymentMethod method : list){
            if(amount > method.getMaxAllowedAmount() || !method.getStatus().equals(Constants.METHOD_STATUS_OK)){
                toRemove.add(method);
            }
            if(method.getMaxAllowedAmount() > maxAmount)
                maxAmount = method.getMaxAllowedAmount();
        }
        list.removeAll(toRemove);
        return maxAmount;
    }
}
