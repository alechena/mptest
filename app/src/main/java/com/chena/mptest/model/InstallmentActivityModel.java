package com.chena.mptest.model;

import com.chena.mptest.MPTestApplication;
import com.chena.mptest.content.Payment;
import com.chena.mptest.presenter.InstallmentActivityPresenter;
import com.chena.mptest.retrofit.interfaces.InstallmentApiInterface;
import com.chena.mptest.retrofit.results.InstallmentResult;
import com.chena.mptest.utils.Constants;
import com.chena.mptest.utils.Functions;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public class InstallmentActivityModel {

    private InstallmentActivityPresenter presenter;
    @Inject Retrofit retrofit;

    public InstallmentActivityModel(InstallmentActivityPresenter presenter) {
        this.presenter = presenter;
        MPTestApplication.getInstance().getNetComponent().inject(this);
    }

    public void getInstallments(Payment payment){
        InstallmentApiInterface paymentMethodApiInterface
                = retrofit.create(InstallmentApiInterface.class);
        Call<InstallmentResult> call;
        if(payment.getBank()!=null) {
            call = paymentMethodApiInterface.
                    getInstallments(Functions.convertAmountToInt(payment.getAmount()), payment.getPaymentMethod().getId(), payment.getBank().getId());
        }else{
            call = paymentMethodApiInterface.
                    getInstallments(Functions.convertAmountToInt(payment.getAmount()), payment.getPaymentMethod().getId());
        }
        call.enqueue(new Callback<InstallmentResult>() {
            @Override
            public void onResponse(Call<InstallmentResult> call, Response<InstallmentResult> response) {
                if ((response.code() == Constants.STATUS_OK) && (response.body() != null))
                    presenter.onSuccess(response.body().getItems());
                else
                    presenter.onError();
            }

            @Override
            public void onFailure(Call<InstallmentResult> call, Throwable t) {
                presenter.onError();
            }
        });
    }

}
