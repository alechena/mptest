package com.chena.mptest.model;

import com.chena.mptest.content.Payment;
import com.chena.mptest.presenter.AmountActivityPresenter;
import com.chena.mptest.utils.Functions;

/**
 * Created by Alejandro Chena on 19/02/2018.
 */

public class AmountActivityModel {

    private AmountActivityPresenter presenter;
    private Payment payment;

    public AmountActivityModel(AmountActivityPresenter presenter) {
        this.presenter = presenter;
    }

    public void validateAmount(String amount){
        try{
            int cleanInt = Functions.convertAmountToInt(amount);
            if(cleanInt > 0) {
                payment = new Payment();
                payment.setAmount(amount);
                presenter.amountValid(payment);
            }else
                presenter.amountInvalid();
        }catch (NumberFormatException e){
            presenter.amountInvalid();
        }
    }

}
