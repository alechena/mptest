package com.chena.mptest.content;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alejandro Chena on 19/02/2018.
 */

public class PaymentMethod implements Parcelable {
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("status")
    private String status;
    @SerializedName("thumbnail")
    private String thumbnail;
    @SerializedName("max_allowed_amount")
    private int maxAllowedAmount;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public int getMaxAllowedAmount() {
        return maxAllowedAmount;
    }

    protected PaymentMethod(Parcel in) {
        id = in.readString();
        name = in.readString();
        status = in.readString();
        thumbnail = in.readString();
        maxAllowedAmount = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(status);
        dest.writeString(thumbnail);
        dest.writeInt(maxAllowedAmount);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PaymentMethod> CREATOR = new Parcelable.Creator<PaymentMethod>() {
        @Override
        public PaymentMethod createFromParcel(Parcel in) {
            return new PaymentMethod(in);
        }

        @Override
        public PaymentMethod[] newArray(int size) {
            return new PaymentMethod[size];
        }
    };
}
