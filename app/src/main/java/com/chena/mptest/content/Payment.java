package com.chena.mptest.content;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public class Payment implements Parcelable {

    private String amount;
    private PaymentMethod paymentMethod;
    private Bank bank;
    private Installment installment;

    public Payment() {
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public Installment getInstallment() {
        return installment;
    }

    public void setInstallment(Installment installment) {
        this.installment = installment;
    }

    protected Payment(Parcel in) {
        amount = in.readString();
        paymentMethod = (PaymentMethod) in.readValue(PaymentMethod.class.getClassLoader());
        bank = (Bank) in.readValue(Bank.class.getClassLoader());
        installment = (Installment) in.readValue(Installment.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(amount);
        dest.writeValue(paymentMethod);
        dest.writeValue(bank);
        dest.writeValue(installment);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Payment> CREATOR = new Parcelable.Creator<Payment>() {
        @Override
        public Payment createFromParcel(Parcel in) {
            return new Payment(in);
        }

        @Override
        public Payment[] newArray(int size) {
            return new Payment[size];
        }
    };
}