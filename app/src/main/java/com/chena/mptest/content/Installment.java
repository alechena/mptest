package com.chena.mptest.content;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public class Installment implements Parcelable {

    private int installments;
    private String recommendedMessage;

    public Installment() {
    }

    public int getInstallments() {
        return installments;
    }

    public void setInstallments(int installments) {
        this.installments = installments;
    }

    public String getRecommendedMessage() {
        return recommendedMessage;
    }

    public void setRecommendedMessage(String recommendedMessage) {
        this.recommendedMessage = recommendedMessage;
    }

    protected Installment(Parcel in) {
        installments = in.readInt();
        recommendedMessage = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(installments);
        dest.writeString(recommendedMessage);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Installment> CREATOR = new Parcelable.Creator<Installment>() {
        @Override
        public Installment createFromParcel(Parcel in) {
            return new Installment(in);
        }

        @Override
        public Installment[] newArray(int size) {
            return new Installment[size];
        }
    };
}
