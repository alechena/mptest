package com.chena.mptest.dagger.component;

import com.chena.mptest.dagger.module.AppModule;
import com.chena.mptest.dagger.module.NetModule;
import com.chena.mptest.model.BankActivityModel;
import com.chena.mptest.model.InstallmentActivityModel;
import com.chena.mptest.model.PaymentMethodActivityModel;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Alejandro Chena on 19/02/2018.
 */

@Singleton
@Component(modules = {NetModule.class, AppModule.class})
public interface NetComponent {
    void inject (PaymentMethodActivityModel paymentMethodActivityModel);
    void inject (BankActivityModel bankActivityModel);
    void inject (InstallmentActivityModel installmentActivityModel);
}
