package com.chena.mptest.activity;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.chena.mptest.R;
import com.chena.mptest.activity.dialog.SummaryDialog;
import com.chena.mptest.content.Payment;
import com.chena.mptest.model.AmountActivityModel;
import com.chena.mptest.presenter.AmountActivityPresenter;
import com.chena.mptest.utils.Constants;
import com.chena.mptest.utils.CustomEditText;
import com.chena.mptest.utils.MoneyTextWatcher;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AmountActivity extends ParentActivity implements AmountActivityPresenter{

    @BindView(R.id.etAmount) CustomEditText etAmount;
    private AmountActivityModel model;
    private Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amount);

        ButterKnife.bind(this);

        tvToolbarTitle.setText(getString(R.string.enter_amount));

        model = new AmountActivityModel(this);

        etAmount.addTextChangedListener(new MoneyTextWatcher(etAmount));
        animation = AnimationUtils.loadAnimation(this, R.anim.anim_edittex);

        etAmount.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER))
                {
                    next();
                }
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(getIntent().getExtras()!=null && getIntent().getExtras().get(Constants.PAYMENT)!=null){
            SummaryDialog dialog = SummaryDialog.newInstance((Payment) getIntent().getParcelableExtra(Constants.PAYMENT));
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            dialog.setCancelable(false);
            dialog.show(ft, "Dialog");
            getIntent().removeExtra(Constants.PAYMENT);
            etAmount.setText("$0");
        }
    }

    @OnClick(R.id.btnNext)
    public void next(){
        model.validateAmount(etAmount.getText().toString());
    }

    @Override
    public void amountValid(Payment payment) {
        PaymentMethodActivity.show(this, PaymentMethodActivity.class, payment);
    }

    @Override
    public void amountInvalid() {
        etAmount.startAnimation(animation);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (intent != null)
            setIntent(intent);
    }
}
