package com.chena.mptest.activity.listener;

import com.chena.mptest.content.Bank;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public interface BankListener {

    void onBankSelected(Bank bank);

}
