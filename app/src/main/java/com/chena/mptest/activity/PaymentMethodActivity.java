package com.chena.mptest.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.chena.mptest.R;
import com.chena.mptest.activity.adapter.PaymentMethodAdapter;
import com.chena.mptest.activity.listener.PaymentMethodListener;
import com.chena.mptest.content.Payment;
import com.chena.mptest.content.PaymentMethod;
import com.chena.mptest.model.PaymentMethodActivityModel;
import com.chena.mptest.presenter.PaymentMethodActivityPresenter;
import com.chena.mptest.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alejandro Chena on 19/02/2018.
 */

public class PaymentMethodActivity extends ParentActivity implements PaymentMethodActivityPresenter, PaymentMethodListener{

    @BindView(R.id.pbLoadingMethods) ProgressBar pbLoading;
    @BindView(R.id.rvMethods) RecyclerView rvMethods;
    @BindView(R.id.tvAmount) TextView tvAmount;
    @BindView(R.id.tvMaxAmount) TextView tvMaxAmount;
    @BindView(R.id.llNoResult) LinearLayout llNoResult;
    private PaymentMethodActivityModel model;
    private PaymentMethodAdapter adapter;
    private Payment payment;
    private ArrayList<PaymentMethod> items;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);

        payment = getIntent().getParcelableExtra(Constants.PAYMENT);

        ButterKnife.bind(this);

        tvToolbarTitle.setText(getString(R.string.enter_method));
        tvAmount.setText(getString(R.string.amount, payment.getAmount()));

        rvMethods.setHasFixedSize(false);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvMethods.setLayoutManager(layoutManager);
        adapter = new PaymentMethodAdapter(this);
        rvMethods.setAdapter(adapter);

        model = new PaymentMethodActivityModel(this);
        if(savedInstanceState != null)
            onSuccess(savedInstanceState.<PaymentMethod>getParcelableArrayList(Constants.SAVE_STATE_LIST));
        else
            model.getPaymentMethods(payment);
    }

    @Override
    public void onRequestStarted() {
        pbLoading.setVisibility(View.VISIBLE);
        rvMethods.setVisibility(View.GONE);
        llNoResult.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(ArrayList<PaymentMethod> paymentMethods) {
        items = paymentMethods;
        pbLoading.setVisibility(View.GONE);
        rvMethods.setVisibility(View.VISIBLE);
        llNoResult.setVisibility(View.GONE);
        adapter.setItems(items);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onError() {
        pbLoading.setVisibility(View.GONE);
        rvMethods.setVisibility(View.GONE);
        llNoResult.setVisibility(View.GONE);
        showErrorDialog(getString(R.string.error_message_methods));
    }

    @Override
    public void onEmptyResult(String maxAmount) {
        pbLoading.setVisibility(View.GONE);
        rvMethods.setVisibility(View.GONE);
        llNoResult.setVisibility(View.VISIBLE);
        tvMaxAmount.setText(getString(R.string.max_amount, maxAmount));
    }

    @Override
    public void onMethodSelected(PaymentMethod paymentMethod) {
        payment.setPaymentMethod(paymentMethod);
        BankActivity.show(this, BankActivity.class, payment);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(Constants.SAVE_STATE_LIST, items);
        super.onSaveInstanceState(outState);
    }
}
