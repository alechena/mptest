package com.chena.mptest.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.chena.mptest.R;

/**
 * Created by Alejandro Chena on 19/02/2018.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);

        final Activity activity = this;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                AmountActivity.show(activity, AmountActivity.class);
                finish();
            }
        }, 900);
    }
}
