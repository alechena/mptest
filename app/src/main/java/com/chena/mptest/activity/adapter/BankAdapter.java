package com.chena.mptest.activity.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chena.mptest.R;
import com.chena.mptest.activity.holder.BankHolder;
import com.chena.mptest.activity.listener.BankListener;
import com.chena.mptest.content.Bank;

import java.util.List;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public class BankAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private BankListener listener;
    private List<Bank> items;

    public BankAdapter(BankListener listener) {
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bank, parent, false);
        return new BankHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((BankHolder) holder).populate(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items!= null ? items.size() : 0;
    }

    public void setItems(List<Bank> items) {
        this.items = items;
    }
}
