package com.chena.mptest.activity.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chena.mptest.R;
import com.chena.mptest.activity.holder.PaymentMethodHolder;
import com.chena.mptest.activity.listener.PaymentMethodListener;
import com.chena.mptest.content.PaymentMethod;

import java.util.List;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public class PaymentMethodAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private PaymentMethodListener listener;
    private List<PaymentMethod> items;

    public PaymentMethodAdapter(PaymentMethodListener listener) {
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_payment_method, parent, false);
        return new PaymentMethodHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((PaymentMethodHolder) holder).populate(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items!= null ? items.size() : 0;
    }

    public void setItems(List<PaymentMethod> items) {
        this.items = items;
    }
}
