package com.chena.mptest.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chena.mptest.R;
import com.chena.mptest.activity.adapter.InstallmentAdapter;
import com.chena.mptest.activity.listener.InstallmentListener;
import com.chena.mptest.content.Installment;
import com.chena.mptest.content.Payment;
import com.chena.mptest.model.InstallmentActivityModel;
import com.chena.mptest.presenter.InstallmentActivityPresenter;
import com.chena.mptest.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public class InstallmentActivity extends ParentActivity implements InstallmentActivityPresenter, InstallmentListener {

    @BindView(R.id.pbLoadingInstallments) ProgressBar pbLoading;
    @BindView(R.id.rvInstallments) RecyclerView rvInstallments;
    @BindView(R.id.tvBank) TextView tvBank;
    @BindView(R.id.tvPaymentMethod) TextView tvPaymentMethod;
    @BindView(R.id.tvAmount) TextView tvAmount;
    @BindView(R.id.rlBank) RelativeLayout rlBank;
    private InstallmentActivityModel model;
    private Payment payment;
    private InstallmentAdapter adapter;
    private ArrayList<Installment> items;
    private Activity activity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_installment);

        activity = this;

        payment = getIntent().getParcelableExtra(Constants.PAYMENT);

        ButterKnife.bind(this);

        tvToolbarTitle.setText(getString(R.string.enter_installment));
        tvAmount.setText(getString(R.string.amount, payment.getAmount()));
        tvPaymentMethod.setText(getString(R.string.payment_method, payment.getPaymentMethod().getName()));
        if (payment.getBank() != null)
            tvBank.setText(getString(R.string.bank, payment.getBank().getName()));
        else
            rlBank.setVisibility(View.GONE);

        rvInstallments.setHasFixedSize(false);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvInstallments.setLayoutManager(layoutManager);
        adapter = new InstallmentAdapter(this);
        rvInstallments.setAdapter(adapter);

        model = new InstallmentActivityModel(this);
        if(savedInstanceState!=null){
            onSuccess(savedInstanceState.<Installment>getParcelableArrayList(Constants.SAVE_STATE_LIST));
        }else {
            model.getInstallments(payment);
        }
    }

    public void onRequestStarted() {
        pbLoading.setVisibility(View.VISIBLE);
        rvInstallments.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(ArrayList<Installment> installments) {
        items = installments;
        pbLoading.setVisibility(View.GONE);
        rvInstallments.setVisibility(View.VISIBLE);
        adapter = new InstallmentAdapter(this);
        adapter.setItems(items);
        rvInstallments.setAdapter(adapter);
    }

    @Override
    public void onError() {
        pbLoading.setVisibility(View.GONE);
        rvInstallments.setVisibility(View.GONE);
        showErrorDialog(getString(R.string.error_message_installment));
    }

    @Override
    public void onInstallmentSelected(Installment installment) {
        payment.setInstallment(installment);
        launchDialog();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(Constants.SAVE_STATE_LIST, items);
        super.onSaveInstanceState(outState);
    }

    private void launchDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.make_payment)
                .setMessage(R.string.make_payment_message)
                .setPositiveButton(R.string.continue_dialog, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ;
                        AmountActivity.show(activity, AmountActivity.class, payment);
                    }
                })
                .setNegativeButton(R.string.cancel_dialog, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();

    }
}
