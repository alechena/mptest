package com.chena.mptest.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.chena.mptest.R;
import com.chena.mptest.activity.adapter.BankAdapter;
import com.chena.mptest.activity.listener.BankListener;
import com.chena.mptest.content.Bank;
import com.chena.mptest.content.Payment;
import com.chena.mptest.model.BankActivityModel;
import com.chena.mptest.presenter.BankActivityPresenter;
import com.chena.mptest.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public class BankActivity extends ParentActivity implements BankActivityPresenter, BankListener{

    @BindView(R.id.pbLoadingBanks) ProgressBar pbLoading;
    @BindView(R.id.rvBanks) RecyclerView rvBanks;
    @BindView(R.id.tvPaymentMethod) TextView tvPaymentMethod;
    @BindView(R.id.tvAmount) TextView tvAmount;
    private BankActivityModel model;
    private BankAdapter adapter;
    private Payment payment;
    private ArrayList<Bank> items;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank);

        payment = getIntent().getParcelableExtra(Constants.PAYMENT);

        ButterKnife.bind(this);

        tvToolbarTitle.setText(getString(R.string.enter_bank));
        tvAmount.setText(getString(R.string.amount, payment.getAmount()));
        tvPaymentMethod.setText(getString(R.string.payment_method, payment.getPaymentMethod().getName()));

        rvBanks.setHasFixedSize(false);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvBanks.setLayoutManager(layoutManager);
        adapter = new BankAdapter(this);
        rvBanks.setAdapter(adapter);

        model = new BankActivityModel(this);
        if(savedInstanceState!=null)
            onSuccess(savedInstanceState.<Bank>getParcelableArrayList(Constants.SAVE_STATE_LIST));
        else
            model.getBanks(payment);
    }

    @Override
    public void onRequestStarted() {
        pbLoading.setVisibility(View.VISIBLE);
        rvBanks.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(ArrayList<Bank> banks) {
        items = banks;
        pbLoading.setVisibility(View.GONE);
        rvBanks.setVisibility(View.VISIBLE);
        adapter.setItems(items);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onError() {
        pbLoading.setVisibility(View.GONE);
        rvBanks.setVisibility(View.GONE);
        showErrorDialog(getString(R.string.error_message_bank));
    }

    @Override
    public void onEmptyResult() {
        payment.setBank(null);
        Intent intent = new Intent(this, InstallmentActivity.class);
        intent.putExtra(Constants.PAYMENT, payment);
        finish();
        startActivity(intent);
    }

    @Override
    public void onBankSelected(Bank bank) {
        payment.setBank(bank);
        InstallmentActivity.show(this, InstallmentActivity.class, payment);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(Constants.SAVE_STATE_LIST, items);
        super.onSaveInstanceState(outState);
    }
}
