package com.chena.mptest.activity.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chena.mptest.R;
import com.chena.mptest.activity.listener.BankListener;
import com.chena.mptest.content.Bank;
import com.chena.mptest.utils.ImageUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public class BankHolder extends RecyclerView.ViewHolder{

    @BindView(R.id.imgBank) ImageView imgBank;
    @BindView(R.id.tvBank) TextView tvBank;
    private BankListener listener;

    public BankHolder(View itemView, BankListener listener) {
        super(itemView);
        this.listener = listener;
        ButterKnife.bind(this, itemView);
    }

    public void populate(final Bank bank){

        tvBank.setText(bank.getName());
        ImageUtils.loadPicture(itemView.getContext(), imgBank, bank.getThumbnail());

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onBankSelected(bank);
            }
        });
    }
}

