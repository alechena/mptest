package com.chena.mptest.activity.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chena.mptest.R;
import com.chena.mptest.activity.holder.InstallmentHolder;
import com.chena.mptest.activity.listener.InstallmentListener;
import com.chena.mptest.content.Installment;

import java.util.List;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public class InstallmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private InstallmentListener listener;
    private List<Installment> items;

    public InstallmentAdapter(InstallmentListener listener) {
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_installment, parent, false);
        return new InstallmentHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((InstallmentHolder) holder).populate(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items!= null ? items.size() : 0;
    }

    public void setItems(List<Installment> items) {
        this.items = items;
    }
}
