package com.chena.mptest.activity.listener;

import com.chena.mptest.content.PaymentMethod;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public interface PaymentMethodListener {

    void onMethodSelected(PaymentMethod paymentMethod);

}
