package com.chena.mptest.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.chena.mptest.R;
import com.chena.mptest.content.Payment;
import com.chena.mptest.utils.Constants;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Alejandro Chena on 19/02/2018.
 */

public class ParentActivity extends AppCompatActivity {

    @BindView(R.id.goback) ImageView imgGoBack;
    @BindView(R.id.toolbar_title) TextView tvToolbarTitle;

    public static void show(Activity activity, Class classActivity, Payment payment) {
        Intent intent = new Intent(activity, classActivity);
        intent.putExtra(Constants.PAYMENT, payment);
        activity.startActivity(intent);
    }

    public static void show(Activity activity, Class classActivity) {
        Intent intent = new Intent(activity, classActivity);
        activity.startActivity(intent);
    }

    @OnClick(R.id.goback)
    public void goBack() {
        finish();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.anim_enter, R.anim.anim_exit);
    }

    @Override
    public void startActivity(Intent intent, Bundle options) {
        super.startActivity(intent, options);
        overridePendingTransition(R.anim.anim_enter, R.anim.anim_exit);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.anim_reventer, R.anim.anim_revexit);
    }

    public void showErrorDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.error_title)
                .setCancelable(false)
                .setMessage(message)
                .setPositiveButton(R.string.back, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .show();
    }
}
