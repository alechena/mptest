package com.chena.mptest.activity.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chena.mptest.R;
import com.chena.mptest.activity.listener.InstallmentListener;
import com.chena.mptest.content.Installment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public class InstallmentHolder extends RecyclerView.ViewHolder{

    @BindView(R.id.tvInstallment) TextView tvInstallment;
    private InstallmentListener listener;

    public InstallmentHolder(View itemView, InstallmentListener listener) {
        super(itemView);
        this.listener = listener;
        ButterKnife.bind(this, itemView);
    }

    public void populate(final Installment installment){

        tvInstallment.setText(installment.getRecommendedMessage());

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onInstallmentSelected(installment);
            }
        });
    }
}
