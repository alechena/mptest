package com.chena.mptest.activity.listener;

import com.chena.mptest.content.Installment;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public interface InstallmentListener {

    void onInstallmentSelected(Installment installment);

}
