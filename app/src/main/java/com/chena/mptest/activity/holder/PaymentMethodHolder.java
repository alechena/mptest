package com.chena.mptest.activity.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chena.mptest.R;
import com.chena.mptest.activity.listener.PaymentMethodListener;
import com.chena.mptest.content.PaymentMethod;
import com.chena.mptest.utils.ImageUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public class PaymentMethodHolder extends RecyclerView.ViewHolder{

    @BindView(R.id.imgPayment) ImageView imgPayment;
    @BindView(R.id.tvPayment) TextView tvPayment;
    private PaymentMethodListener listener;

    public PaymentMethodHolder(View itemView, PaymentMethodListener listener) {
        super(itemView);
        this.listener = listener;
        ButterKnife.bind(this, itemView);
    }

    public void populate(final PaymentMethod paymentMethod){

        tvPayment.setText(paymentMethod.getName());
        ImageUtils.loadPicture(itemView.getContext(), imgPayment, paymentMethod.getThumbnail());

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onMethodSelected(paymentMethod);
            }
        });
    }
}
