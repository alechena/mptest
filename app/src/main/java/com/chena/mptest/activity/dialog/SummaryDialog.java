package com.chena.mptest.activity.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chena.mptest.R;
import com.chena.mptest.content.Payment;
import com.chena.mptest.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public class SummaryDialog extends DialogFragment{

    @BindView(R.id.tvBank) TextView tvBank;
    @BindView(R.id.tvPaymentMethod) TextView tvPaymentMethod;
    @BindView(R.id.tvAmount) TextView tvAmount;
    @BindView(R.id.tvInstallment) TextView tvInstallment;
    @BindView(R.id.btnFinish) Button btnFinish;
    @BindView(R.id.rlBank) RelativeLayout rlBank;

    public static SummaryDialog newInstance(Payment payment) {
        SummaryDialog summaryDialog = new SummaryDialog();

        Bundle args = new Bundle();
        args.putParcelable(Constants.PAYMENT, payment);
        summaryDialog.setArguments(args);

        return summaryDialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_summary, container, false);
        ButterKnife.bind(this, v);

        Payment payment = getArguments().getParcelable(Constants.PAYMENT);

        tvAmount.setText(getString(R.string.amount, payment.getAmount()));
        tvPaymentMethod.setText(getString(R.string.payment_method, payment.getPaymentMethod().getName()));
        tvInstallment.setText(getString(R.string.total, payment.getInstallment().getRecommendedMessage()));

        if(payment.getBank()!=null)
            tvBank.setText(getString(R.string.bank, payment.getBank().getName()));
        else
            rlBank.setVisibility(View.GONE);

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return v;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }
}
