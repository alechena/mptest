package com.chena.mptest.retrofit.interfaces;

import com.chena.mptest.content.Bank;
import com.chena.mptest.utils.Constants;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public interface BankApiInterface {

    @GET(Constants.BANK_URL)
    Call<ArrayList<Bank>> getPaymentMethods(@Query("payment_method_id") String paymentMethod);

}
