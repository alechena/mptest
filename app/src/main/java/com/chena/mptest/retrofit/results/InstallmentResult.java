package com.chena.mptest.retrofit.results;

import com.chena.mptest.content.Installment;

import java.util.ArrayList;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public class InstallmentResult {

    private ArrayList<Installment> items;

    public ArrayList<Installment> getItems() {
        return items;
    }

    public void setItems(ArrayList<Installment> items) {
        this.items = items;
    }
}
