package com.chena.mptest.retrofit.interfaces;

import com.chena.mptest.content.PaymentMethod;
import com.chena.mptest.utils.Constants;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Alejandro Chena on 19/02/2018.
 */

public interface PaymentMethodApiInterface {

    @GET(Constants.PAYMENT_METHOD_URL)
    Call<ArrayList<PaymentMethod>> getPaymentMethods();

}
