package com.chena.mptest.retrofit.interfaces;

import com.chena.mptest.retrofit.results.InstallmentResult;
import com.chena.mptest.utils.Constants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public interface InstallmentApiInterface {

    @GET(Constants.INSTALLMENT_URL)
    Call<InstallmentResult> getInstallments(@Query("amount") int amount, @Query("payment_method_id") String payment, @Query("issuer.id") String bank);

    @GET(Constants.INSTALLMENT_URL)
    Call<InstallmentResult> getInstallments(@Query("amount") int amount, @Query("payment_method_id") String payment);

}
