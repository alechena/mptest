package com.chena.mptest.utils;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public class Functions {

    public static int convertAmountToInt(String amount)throws NumberFormatException{
        String cleanString = amount.toString().trim().replaceAll("[$,.]", "");
        return Integer.valueOf(cleanString);
    }

}
