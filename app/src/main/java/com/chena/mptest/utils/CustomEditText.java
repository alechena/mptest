package com.chena.mptest.utils;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by Alejandro Chena on 19/02/2018.
 */

public class CustomEditText extends android.support.v7.widget.AppCompatEditText{
    public CustomEditText(Context context) {
        super(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        this.setSelection(getText().length());
    }
}
