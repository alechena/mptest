package com.chena.mptest.utils;

import com.chena.mptest.BuildConfig;

/**
 * Created by Alejandro Chena on 19/02/2018.
 */

public class Constants {

    public static final String BASE_URL = BuildConfig.BASE_URL;
    public static final String API_KEY = "444a9ef5-8a6b-429f-abdf-587639155d88";
    public static final String PAYMENT_METHOD_URL = BASE_URL + "payment_methods?public_key=" + API_KEY;
    public static final String BANK_URL = BASE_URL + "payment_methods/card_issuers?public_key=" + API_KEY;
    public static final String INSTALLMENT_URL = BASE_URL + "payment_methods/installments?public_key=" + API_KEY;
    public static final int STATUS_OK = 200;
    public static final String METHOD_STATUS_OK = "active";

    //BUNDLE CONSTANTS
    public static final String PAYMENT = "PAYMENT";
    public static final String SAVE_STATE_LIST = "SAVE_STATE_LIST";

}
