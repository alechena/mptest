package com.chena.mptest.utils;

import android.content.Context;
import android.widget.ImageView;

import com.chena.mptest.R;
import com.squareup.picasso.Picasso;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public class ImageUtils {

    public static void loadPicture(Context context, ImageView imageView, String url){
        if(url!=null && !url.trim().isEmpty()){
            Picasso.with(context)
                    .load(url)
                    .error(R.drawable.ic_card_error)
                    .into(imageView);
        }
    }

}
