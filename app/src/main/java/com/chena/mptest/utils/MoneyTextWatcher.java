package com.chena.mptest.utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Alejandro Chena on 19/02/2018.
 */

public class MoneyTextWatcher implements TextWatcher {
    private final EditText editText;
    private String current = "";
    private static final int MAX_AMOUNT = 999999;
    private Locale currentLocale;
    private NumberFormat nf;

    public MoneyTextWatcher(EditText editText) {
        this.editText = editText;
        currentLocale = Locale.getDefault();
        nf = NumberFormat.getNumberInstance(currentLocale);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (!s.toString().equals(current)) {
            editText.removeTextChangedListener(this);

            String cleanString = s.toString().trim().replaceAll("[$,.]", "");

            if(!cleanString.isEmpty()) {
                try {
                    int cleanInt = Integer.valueOf(cleanString);
                    if(cleanInt <= MAX_AMOUNT) {
                        String res = "$" + nf.format(cleanInt);
                        current = res;
                        editText.setText(res);
                    }else{
                        editText.setText(current);
                    }
                } catch (NumberFormatException e) {
                    editText.setText(current);
                }
            }else{
                current = "$0";
                editText.setText(current);
            }
            editText.addTextChangedListener(this);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {
    }
}
