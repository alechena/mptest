package com.chena.mptest.presenter;

import com.chena.mptest.content.Bank;

import java.util.ArrayList;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public interface BankActivityPresenter {

    void onRequestStarted();

    void onSuccess(ArrayList<Bank> banks);

    void onError();

    void onEmptyResult();
}
