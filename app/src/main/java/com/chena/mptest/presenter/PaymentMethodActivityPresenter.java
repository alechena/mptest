package com.chena.mptest.presenter;

import com.chena.mptest.content.PaymentMethod;

import java.util.ArrayList;

/**
 * Created by Alejandro Chena on 19/02/2018.
 */

public interface PaymentMethodActivityPresenter {

    void onRequestStarted();

    void onSuccess(ArrayList<PaymentMethod> paymentMethods);

    void onError();

    void onEmptyResult(String maxAmount);
}
