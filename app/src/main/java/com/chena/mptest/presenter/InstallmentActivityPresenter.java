package com.chena.mptest.presenter;

import com.chena.mptest.content.Installment;

import java.util.ArrayList;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public interface InstallmentActivityPresenter {

    void onRequestStarted();

    void onSuccess(ArrayList<Installment> installments);

    void onError();

}
