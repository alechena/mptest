package com.chena.mptest.presenter;

import com.chena.mptest.content.Payment;

/**
 * Created by Alejandro Chena on 19/02/2018.
 */

public interface AmountActivityPresenter {

    void amountValid(Payment payment);
    void amountInvalid();

}
