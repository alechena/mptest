package com.chena.mptest.parser;

import com.chena.mptest.content.Installment;
import com.chena.mptest.retrofit.results.InstallmentResult;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Alejandro Chena on 20/02/2018.
 */

public class InstallmentsParser implements JsonDeserializer<InstallmentResult> {

    @Override
    public InstallmentResult deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        InstallmentResult result = new InstallmentResult();
        ArrayList<Installment> installments = new ArrayList<>();

        try {
            JsonArray jsonArray = json.getAsJsonArray();
            for(int i=0 ; i < jsonArray.size(); i++){
                JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
                JsonArray itemsJson = jsonObject.getAsJsonArray("payer_costs");
                if (itemsJson != null && itemsJson.size() > 0) {
                    for(int a = 0; a < itemsJson.size(); a++){
                        installments.add(parseInstallment(itemsJson.get(a).getAsJsonObject()));
                    }
                }
            }
            result.setItems(installments);
            return result;
        }catch (JsonParseException e){
            return result;
        }
    }

    private Installment parseInstallment(JsonObject jsonObject){
        Installment installment = new Installment();
        installment.setInstallments(jsonObject.get("installments").getAsInt());
        installment.setRecommendedMessage(jsonObject.get("recommended_message").getAsString());
        return installment;
    }
}
